# Abdal WP XmlRPC BruteForce

## 🎤 README Translation
- [English](README.md)
- [فارسی](README.fa.md)

## 👀 Screenshot

![](screenshot.jpg)
![](https://github.com/ebrasha/abdal-wp-xmlrpc-bruteforce/blob/main/scshot/app-ui.jpg)


 ## 💎 General purpose
Abdal WP XmlRPC BruteForce is a tool designed to test the security of WordPress sites by attempting to crack access via the XML-RPC interface

 ## ⚓ Prerequisites for programmers
>Telerik

## ✨ Features

- Supports multi-million password lists
- Ability to retrieve username lists for multi-user sites
- Detection of your account being blocked
- Detection of the target running WordPress
- Detection of XMLRPC authentication availability
- Voice messaging
- Error-free in compromised accounts
- Free from spyware and any kind of malicious code
- Completely free and open source
- Very high speed
- User-friendly and attractive interface

## 📝️ How it Works?
Provide the software with the password list and username list, enter the desired website address, and press the Start button. To launch new attacks, close the software and reopen it.

## ❤️ Donation

> USDT:      TXLasexoQTjKMoWarikkfYRYWWXtbaVadB

> BTC :   bc1q9w9ymgz2wluax60rsuza4q0at7gpy82g8el6zj

> ETH :   0x402b9f67091Af07224286F16d7377bc50268Db94

> For Iranian People -> MellatBank : 6104-3378-5301-4247

## 🤵 Programmer
Handcrafted with Passion by Ebrahim Shafiei (EbraSha)

E-Mail = Prof.Shafiei@Gmail.com

Telegram: https://t.me/ProfShafiei

## ☠️ Reporting Issues

If you are facing a configuration issue or something is not working as you expected to be, please use the **Prof.Shafiei@Gmail.com** . Issues on GitLab  or Github are also welcomed.


